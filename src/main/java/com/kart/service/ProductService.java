package com.kart.service;

import java.util.List;

import com.kart.entity.Product;


public interface ProductService {
	
	List<Product> getAllProducts();

	List<Product> findAllByIds(Iterable<Long> ids);

	Product saveAndFlush(Product entity);
	
	List<Product> saveAll(Iterable<Product> entities);
	
	void flush();
	
	Product getOne(Long id);
	
	List<Product> findByProductName(String productName);
	
	List<Product> findBySeller(String seller);

	List<Product> findByOffers(String offers);

	List<Product> findByProductCategory(String productCategory);
	
	void deleteInBatch(Iterable<Product> entities);

	void deleteAllInBatch();
	
}
