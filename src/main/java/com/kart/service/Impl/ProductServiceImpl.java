/**
 * 
 */
package com.kart.service.Impl;

import java.util.List;
  
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kart.dao.ProductRepository;
import com.kart.entity.Product;
import com.kart.service.ProductService;


/**
 * @author LENOVO
 *
 */

@Service
public class ProductServiceImpl implements ProductService {
	
	@Autowired
	private ProductRepository productRepository;

	public List<Product> getAllProducts() {
		return productRepository.findAll();
	}

	public List<Product> findAllByIds(Iterable<Long> ids) {
		return productRepository.findAllById(ids);
	}
	
	public List<Product> findByProductName(String productName) {
		return productRepository.findByProductName(productName);
	}
	
	public Product getOne(Long id) {
		return productRepository.getOne(id);
	}
	
	public Product saveAndFlush(Product entity) {
		return productRepository.saveAndFlush(entity);
	}

	public void flush() {
		productRepository.flush();
	}
	
	public List<Product> saveAll(Iterable<Product> entities) {
		return productRepository.saveAll(entities);
	}

	public void deleteInBatch(Iterable<Product> entities) {
		productRepository.deleteInBatch(entities);
	}

	public void deleteAllInBatch() {
		productRepository.deleteAllInBatch();
	}

	public List<Product> findBySeller(String seller) {
		return productRepository.findBySeller(seller);
	}

	public List<Product> findByOffers(String offers) {
		return productRepository.findByOffers(offers);
	}

	public List<Product> findByProductCategory(String productCategory) {
		return productRepository.findByProductCategory(productCategory);
	}
	
}
