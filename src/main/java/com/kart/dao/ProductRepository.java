package com.kart.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.kart.entity.Product;

@Repository
public interface ProductRepository extends JpaRepository<Product, Long>, CrudRepository<Product, Long> {
	
	List<Product> findByProductName(String productName);

	List<Product> findBySeller(String seller);

	List<Product> findByOffers(String offers);

	List<Product> findByProductCategory(String productCategory);
	
}
