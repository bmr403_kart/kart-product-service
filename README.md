# kart-product-service ![CI status](https://img.shields.io/badge/build-passing-brightgreen.svg)

kart-product-service is a java library of manufacturing unit application product service layer to integrate with  DB model and also Eureka Netflix OSS service discovery to collaborate other services over the app.

## Build

* mvn clean install


## Installation

* mvn clean install

### Requirements

* Java 8
* Maven ~> 3
* Git ~> 2


## Usage

```
<dependency>
    <groupId>com.kart</groupId>
    <artifactId>product-service</artifactId>
    <version>0.0.1-SNAPSHOT</version>
</dependency>
```

## Development


## Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.




Manufacturing unit application productservice layer to integrate with Eureka Netflix OSS service discovery to collaborate other services over the app.